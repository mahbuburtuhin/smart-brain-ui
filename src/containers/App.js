import React, {Component} from 'react';
import Particles from 'react-particles-js';
import './App.css';
import Navigation from '../components/Navigation/Navigation';
import Register from '../components/Register/Register';
import Signin from '../components/Signin/Signin';
import Rank from '../components/Rank/Rank';
import LinkBox from '../components/LinkBox/LinkBox';
import Logo from '../components/Logo/Logo';

const particlesOptions = {
  particles: {
    number: {
      value: 30,
      density: {
        enable: true,
        value_area: 800
      }
    }
  }
}

class App extends Component {
  constructor(){
    super();
    this.state = {
      route: 'signin',
      user: {
        name: '',
        entries: '',
      }
    }
  }
  onRouteChange = (route) => {
    this.setState({ route : route});
  }

  loadUser = (data) => {
    this.setState({
      user:  {
        id: data.id,
        name: data.name,
        email: data.email,
        entries: data.entries,
        joined: data.joined
      }
    });
  }

  render() {
    const { route } = this.state;
    return (
      <div className= 'App'>
        <Particles className='particles'
        params={particlesOptions}
      />
        <Navigation route={route} onRouteChange={this.onRouteChange} />
        <Logo/>
        {
          route === 'home' ?
            <div>
              <Rank user={this.state.user} />
              <LinkBox/>
            </div>
            :(route === 'signin' ? <Signin loadUser={this.loadUser} onRouteChange={this.onRouteChange}  />
            : (route === 'register' ? <Register loadUser={this.loadUser} onRouteChange={this.onRouteChange}  /> : ''))           
        }
        
      </div>
    );
  }
}

export default App;
