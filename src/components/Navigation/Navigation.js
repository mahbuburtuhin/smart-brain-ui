import React from 'react';

const Navigation = (props) => {
  const { route, onRouteChange } = props;

  return (
    <nav style={{ display: 'flex', justifyContent: 'flex-end' }}>
      {((route === 'home')?
      <p className='f3 link dim black underline pa4 pointer' onClick={()=>onRouteChange('signin')}>Sign-out</p>:
      ((route === 'register') ?
      <p className='f3 link dim black underline pa4 pointer' onClick={()=>onRouteChange('signin')}>Signin</p> :
      <p className='f3 link dim black underline pa4 pointer' onClick={() => onRouteChange('register')}>Register</p>))}
    </nav>
    );
}


export default Navigation;